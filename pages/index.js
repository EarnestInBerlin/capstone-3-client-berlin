import Head from 'next/head'
import styles from '../styles/Home.module.css'
import NavBar from '../components/NavBar' 
import Footer from '../components/Footer' 
import {Fragment} from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'

export default function Home() {
  return (
    <Fragment>
      <div className={styles.body}>
        <div className={styles.container}>
         
          <main className={styles.main}>
           
              <h1 className={styles.title}>
                &#8499;oney, &#8499;oney, &#8499;oney!</h1>

              <p className={styles.description}>
                Ain't it Funny? It's a budget tracking app!
              </p>
            

              <div className={styles.grid}>
                <a href="/register" className={styles.card}>
                  <h3>Register &rarr;</h3>
                  <p>New to &#8499; &#8499; &#8499;? Create an account and start budgeting now!</p>
                </a>

                <a href="/login" className={styles.card}>
                  <h3>Log in &rarr;</h3>
                  <p>Have an account already? Login here!</p>
                </a>
              </div>
          </main>

        
        </div>
      </div>
    </Fragment>  
  )
}
