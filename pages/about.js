import {Card, Button,Container} from 'react-bootstrap'
import {useEffect, useContext, useState} from 'react'
import Swal from 'sweetalert2'
import Router from 'next/router'
import 'bootstrap/dist/css/bootstrap.min.css'
import Image from 'next/image'


export default function About(){

	return(
		
			<Card className="aboutPage my-5">
			   <Card.Body className="text-center">
			   <Image src="/cuteme.PNG" alt="Creator of Money Money Money" width={500} height={700} className="ABBA"/>
			    <Card.Title>&#8499;oney, &#8499;oney, &#8499;oney!</Card.Title>
			    <Card.Subtitle className="mb-2 text-muted">Ain't it funny?</Card.Subtitle>
				    <Card.Text>
					    <p>I work all night, I work all day, to pay the bills I have to pay! Ain't it sad? And still there never seems to be a single penny left for me. That's too bad! In my dreams I have a plan, if I got me a wealthy man... I wouldn't have to work at all, I'd fool around and have a ball.</p>
							
						<p>Hi! My name is Ernesse Berlin. I'm a struggling artist and dev- by that I mean I'm struggling to master either endeavors. This is a simple budget tracking app you can use to make spending habit less... spend-y. I hope you'll like it!</p>
			    	</Card.Text>
			    <Card.Link href="https://www.instagram.com/hyde.strange/">Semi Decent Art</Card.Link>
			    <Card.Link href="https://youtu.be/QJK6QixTu64">Good music</Card.Link>
			  </Card.Body>
			</Card>
		
	)
}
