import {useState, useEffect, useContext} from 'react'
import {Form, Button,Container} from 'react-bootstrap'
import Swal from 'sweetalert2'
import Router from 'next/router'
import UserContext from '../userContext'
import {GoogleLogin} from 'react-google-login'

export default function Login(){

	const {user, setUser} = useContext(UserContext)
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [isActive, setIsActive] = useState(true)



	useEffect(()=>{

		if(email !== "" && password !== ""){
			setIsActive(true)
		}else{
			setIsActive(false)
		}

	},[email, password])



	function authenticate(e){

		e.preventDefault();


		fetch('https://gentle-inlet-26893.herokuapp.com/api/users/login',{

			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({

				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data =>{

			if(data.accessToken){

				//token lang ang kailangan natin
				localStorage.setItem("token",data.accessToken)

				fetch('https://gentle-inlet-26893.herokuapp.com/api/users/details',{

					headers:{

						Authorization: `Bearer ${data.accessToken}`
					}
				})
				.then(res=>res.json())
				.then(data =>{

					console.log(email)
					localStorage.setItem('email',data.email)
					localStorage.setItem('id',data._id)					

					//after getting the details from the API server, we will set the global user state. Pag ka log in pa lang, iupdate na natin yung user state.
					setUser({

						email: data.email,
						id: data._id
					})

				})

				Swal.fire({

					icon:"success",
					title:"Successfully Logged in",
					text: "Thank you for logging in."

				})

				// Router component's push method will redirect the user to the endpoint given to the method.
				Router.push('/profile')

			}else{

				Swal.fire({

					icon: "error",
					title: "Unsuccessful Login",
					text: "User authenticaltion has Failed."
				})
			}
			
		})

		//set the  input states into their initial value
		setEmail("");
		setPassword("");

	}

	function authenticateGoogleToken(response){
		console.log(response)
		fetch('https://gentle-inlet-26893.herokuapp.com/api/users/verify-google-id-token', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json' 
			}, 
				body: JSON.stringify({
					tokenId: response.tokenId,
					accessToken: response.accessToken
				})
		}) 
		.then(res => res.json())
		.then(data => {
			if(typeof data.accessToken !== 'undefined'){
				localStorage.setItem('token', data.accessToken)
				fetch('https://gentle-inlet-26893.herokuapp.com/api/users/details', {
					headers: {
						'Authorization': `Bearer ${data.accessToken}`
					}
				})
				.then(res => res.json())
				.then(data => {
					localStorage.setItem('email', data.email)
					localStorage.setItem('id', data.id)

					setUser({
						email: data.email,
						id: data._id
					})
					Swal.fire({
						icon: 'success',
						title: 'Successful Login'
					})
					Router.push('/profile')
				})
			} else {
				if(data.error === "google-auth-error"){
					Swal.fire({
						icon: 'error',
						title: 'Google Authentication Failed'
					}) 
				} else if(data.error === "login-type-error") {
					Swal.fire({
						icon: 'error',
						title: 'Login failed',
						text: 'You may have registered through a different procedure'
					})
				}
			}
		})
	}


	return(

			<Container>
			  <Form onSubmit={e => authenticate(e)} className="my-3">
					<Form.Group controlId="userEmail">
						<Form.Label>Email:</Form.Label>
						<Form.Control type="email" placeholder="Enter Email" value={email} onChange={e=> setEmail(e.target.value)} required />
					</Form.Group>
					<Form.Group controlId="userPassword">
						<Form.Label>Password:</Form.Label>
						<Form.Control type="password" placeholder="Enter Email" value={password} onChange={e=> setPassword(e.target.value)} required/>
					</Form.Group>
	 				{
						isActive
						? <Button variant="primary" type="submit" className="btn-block">Submit</Button>
						: <Button variant="primary" type="submit" className="btn-block" disabled>Submit</Button>
				    }

				    <GoogleLogin 
				    	clientId="507133729630-mln3pdorkdv8t6eh3kehjdr8o21c5t06.apps.googleusercontent.com" 
				    	buttonText="Google Login"
				    	onSuccess={authenticateGoogleToken}
				    	onFailure={authenticateGoogleToken}
				    	cookiePolicy={'single_host_origin'}
				    	className="w-100 text-center d-flex justify-content-center my-2 rounded-lg"	/>
		     </Form>
		 </Container>

	    )

}
//line 188
