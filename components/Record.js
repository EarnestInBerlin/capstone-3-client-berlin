import{useState, useEffect, useContext} from 'react'
import {Card} from 'react-bootstrap'
import Moment from 'moment'


// To get the prop, destructure it
export default function Category({recordProp}){


	//  after you get the prop, destructure it para makuha mo yng laman na need mo:
	const {_id, name, type,category, balance, recordedOn, amount, description} = recordProp
	let bago = (Moment(recordedOn).format('LLLL'))

	return(

			<Card className="my-3">
				<Card.Body>
				
					<Card.Title>
						{name}
					</Card.Title>
				
					<Card.Text>
						{type}
					</Card.Text>
					
					<Card.Title>
						{category}
					</Card.Title>
				
					<Card.Text>
					 	&#8369; {balance}
					</Card.Text>

					<Card.Text>
						&#8369; {amount}
					</Card.Text>

					<Card.Text>
						{description}
					</Card.Text>
				
					<Card.Text>
						{recordedOn}
					</Card.Text>
				</Card.Body>
			</Card>
		)
}
