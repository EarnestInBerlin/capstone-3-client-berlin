import{useState, useEffect, useContext} from 'react'
import {Card} from 'react-bootstrap'

export default function Category({categoryProp}){
	const {_id, name, type} = categoryProp

	return(

			<Card className="my-3">
				<Card.Body>
					<Card.Title>
						{name}
					</Card.Title>
					<Card.Text>
						{type}
					</Card.Text>
				</Card.Body>
			</Card>
		)
}
